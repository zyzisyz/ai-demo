# coding=utf-8

# ###################################
# filename: main.py
# author: Yang Zhang (BUPT)
# e-mail: zyziszy@foxmail.com
# time: 2018-10-19
# note: 本代码使用OpenCV识别人脸
# reference:
# https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_objdetect/py_face_detection/py_face_detection.html#face-detection
# ###################################

import cv2
import os

# load the required XML classifiers
face_cascade = cv2.CascadeClassifier(r'./haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier(r'./haarcascade_eye.xml')


def Detection(path):
    img = cv2.imread(path)  # 读取图片为OpenCV的img对象
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # 转化为灰度图

    # 这个函数用于识别人脸
    # gray 是灰度图
    # scaleFactor 是
    # minNeighbors 是
    # minSize 是
    faces = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=3,
        minSize=(5, 5),
    )

    print("{0}发现{1}张人脸!".format(path, len(faces)))

    # 在图片上圈出人脸和眼睛
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
        roi_gray = gray[y:y + h, x:x + w]
        roi_color = img[y:y + h, x:x + w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex, ey, ew, eh) in eyes:
            cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

    cv2.imshow('img', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == '__main__':
    while True:
        path = input("input img path or 'q' for quit:\n")  # 输入图片路径

        if path == 'q' or path == 'Q':
            break
        else:
            if os.path.exists(path):
                Detection(path)
            else:
                print("img is not exist, please try it again...\n")
