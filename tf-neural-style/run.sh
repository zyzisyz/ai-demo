#!/bin/bash

#*************************************************************************
#	> File Name: run.sh
#	> Author: Yang Zhang 
#	> Mail: zyziszy@foxmail.com 
#	> Created Time: Fri 19 Oct 2018 11:05:30 AM CST
# ************************************************************************/


# setting up the conda environment
conda create -n ai-demo -y python=3.6
source activate ai-demo
pip install -U pip
pip install -r requirements.txt

# download model
rm -rf model
mkdir model
cd model
wget -O vgg19_weights.h5 \
    https://github.com/fchollet/deep-learning-models/releases/download/v0.1/vgg19_weights_tf_dim_ordering_tf_kernels.h5

cd ..

# get path
echo -n "input content_image path: "
read content_image
echo -n "input style_image path: "
read style_image

# run the python code
python main.py --content_image ${content_image} --style_image ${style_image}
