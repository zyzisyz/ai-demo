# SVM Visualization

这是一个简单的二分类任务，目的就是使两类样本之间的间隔大一些。采用**线性核函数**，选取了三个特征，变成了一个三维空间的二分类任务。

我是copy[网友（@TensorSense）的CSDN博客](https://blog.csdn.net/u011995719/article/details/81157193)

## 如何运行

参考`requirements.txt`安装所需python包。例如：

```bash
pip install -r requirements.txt
```

之后在当前路径的终端（bash或者powershell)中输入

```bash
python main.py
```

PS: Linux可能需要开启GUI，我没在服务器上试过