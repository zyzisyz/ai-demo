# coding=utf-8

# ###################################
# filename: main.py
# author: Yang Zhang (BUPT)
# e-mail: zyziszy@foxmail.com
# time: 2018-10-19
# note: 牛顿法（二维）
# ###################################

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation
import random
from time import sleep

# subplots返回的值的类型为元组
# 其中包含两个元素
# 第一个为一个画布
# 第二个是子图
fig, ax = plt.subplots()

# 绘制函数
# 函数为 y = x - pow(x, 2) + 2 * pow(x, 3) - 0.5 * pow(x, 4) + 0.75
# 其导数为 y = 1 - 2 * x + 6 * pow(x, 2) - 2 * pow(x, 3)
x = np.linspace(-10, 10, 500)
y = x - pow(x, 2) + 2 * pow(x, 3) - 0.5 * pow(x, 4) + 0.75
plt.plot(x, y)

# 随机生成position
# p是全局变量
p = random.randint(0, 4)


# 牛顿法
def Newton_method():
    global p
    k = 1 - 2 * p + 6 * pow(p, 2) - 2 * pow(p, 3)
    y = p - pow(p, 2) + 2 * pow(p, 3) - 0.5 * pow(p, 4) + 0.75
    x = -y/k + p
    y = x - pow(x, 2) + 2 * pow(x, 3) - 0.5 * pow(x, 4) + 0.75
    p = x
    return x, y, k


# 用于更新的函数
def animate(frame):
    x = 1
    x0, y0, k = Newton_method()
    if y0 != 0:
        x = np.linspace(-10, 10, 500)
        y = k * (x - x0) + y0
        ax.plot(x, y, animated=False)
        
        


# 设置坐标轴范围
plt.xlim((-10, 10))
plt.ylim((-10, 10))

# 将上、右边框去掉
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

# 设置x和y轴的位置及数据在坐标轴上的位置
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))

ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data', 0))

# 设置坐标轴名称
# plt.xlabel('X')
# plt.ylabel('Y')

# 设置坐标轴刻度
my_x_ticks = np.arange(-10, 10, 1)
my_y_ticks = np.arange(-10, 10, 1)
plt.xticks(my_x_ticks)
plt.yticks(my_y_ticks)


# 参数func是更新图形的函数
# frames是总共更新的次数
# init_func是图形开始使用的函数，
# interval是更新的间隔时间(ms)
# blit决定是更新整张图的点(False)还是只更新变化的点（True）

ani = FuncAnimation(fig=fig, func=animate, repeat=True,
                    frames=1, interval=1000, blit=False)
plt.show()
