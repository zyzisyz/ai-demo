#!/bin/bash

#*************************************************************************
#	> File Name: run.sh
#	> Author: Yang Zhang
#	> Mail: zyziszy@foxmail.com
#	> Created Time: Fri 19 Oct 2018 11:05:30 AM CST
# ************************************************************************/


# setting up the conda environment
conda create -n ai-demo -y python=3.6
source activate ai-demo
pip install -U pip
pip install -r requirements.txt

# run the python code
python main.py